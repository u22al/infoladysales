package dnet.org.bd.kallyanisales.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import java.util.List;

import dnet.org.bd.kallyanisales.R;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.http.ServerConfig;
import dnet.org.bd.kallyanisales.model.ItemType;
import dnet.org.bd.kallyanisales.model.Product;
import dnet.org.bd.kallyanisales.repository.CartRepository;
import dnet.org.bd.kallyanisales.repository.ProductRepository;
import dnet.org.bd.kallyanisales.util.Tools;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/6/2016.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{
    private static List<Product> mList;
    private static Toast toast;
    private Context mContext;

    public ProductAdapter(Context context){
        mContext = context;
        mList = ProductRepository.getAll(context);
    }

    public ProductAdapter(Context context, List<Product> list){
        mContext = context;
        mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View productList = LayoutInflater.from(parent.getContext())
                                            .inflate(R.layout.product_item, parent, false);
        return new ViewHolder(productList);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Product product = mList.get(position);

        holder.textName.setText(product.getName());
        holder.textMrp.setText(product.getMrp()+"");

        String image = ServerConfig.BaseUrl+"/uploads/product/"+Tools.getImagePath(product.getImage());
        Glide.with(mContext).load(image).placeholder(R.drawable.placeholder).into(holder.imageProduct);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView textName;
        public TextView textMrp;
        public ImageView imageProduct;
        public ImageButton btnAdd;

        public ViewHolder(View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.text_item_name);
            textMrp = (TextView) itemView.findViewById(R.id.text_item_mrp);
            imageProduct = (ImageView) itemView.findViewById(R.id.image_item);

            btnAdd = (ImageButton) itemView.findViewById(R.id.btn_add_product);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                Product product = mList.get(getAdapterPosition());

                if(toast!=null){
                    toast.cancel();
                }
                toast =Toast.makeText(view.getContext(), product.getName()+" added.", Toast.LENGTH_SHORT);
                toast.show();

                ContentValues cv = new ContentValues();
                cv.put(DB.SaleItem.COL_ITEM_TYPE, ItemType.PRODUCT);
                cv.put(DB.SaleItem.COL_ITEM_ID, product.getProductId());
                cv.put(DB.SaleItem.COL_QUANTITY, 1);
                cv.put(DB.SaleItem.COL_MRP, product.getMrp());
                cv.put(DB.SaleItem.COL_PURCHASE_PRICE, product.getInfoladyPrice());

                CartRepository.add(view.getContext(), cv);
                }
            });
        }
    }
}
