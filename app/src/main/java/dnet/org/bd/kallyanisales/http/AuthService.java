package dnet.org.bd.kallyanisales.http;

import dnet.org.bd.kallyanisales.model.Credential;
import dnet.org.bd.kallyanisales.response.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public interface AuthService {
    @POST("auth")
    Call<AuthResponse> login(@Body Credential credential);
}
