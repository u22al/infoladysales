package dnet.org.bd.kallyanisales.db;

import android.provider.BaseColumns;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public final class DB{
    private DB(){}

    public static class Product implements BaseColumns, Schema{
        public static final String TABLE = "products";
        public static final String COL_PRODUCT_ID = "product_id";
        public static final String COL_CATEGORY_ID = "category_id";
        public static final String COL_BRAND_ID = "brand_id";
        public static final String COL_NAME = "name";
        public static final String COL_IMAGE = "image";
        public static final String COL_MRP = "mrp";
        public static final String COL_INFOLADY_PRICE = "infolady_price";

        public static final String SQL_CREATE = "CREATE TABLE " + Product.TABLE +" ("
                + Product.COL_PRODUCT_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY + " KEY"+Schema.SEP
                + Product.COL_CATEGORY_ID + Schema.TYPE_INT + Schema.SEP
                + Product.COL_BRAND_ID + Schema.TYPE_INT + Schema.SEP
                + Product.COL_NAME + Schema.TYPE_TEXT + Schema.SEP
                + Product.COL_IMAGE + Schema.TYPE_TEXT + Schema.SEP
                + Product.COL_MRP + Schema.TYPE_REAL + Schema.SEP
                + Product.COL_INFOLADY_PRICE + Schema.TYPE_REAL
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+Product.TABLE;
    }

    public static class Category implements BaseColumns, Schema{
        public static final String TABLE = "categories";
        public static final String COL_CATEGORY_ID = "category_id";
        public static final String COL_NAME = "name";
        public static final String COL_LANG_KEY = "lang_key";

        public static final String SQL_CREATE = "CREATE TABLE "+ Category.TABLE +" ("
                + Category.COL_CATEGORY_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY +" KEY"+ Schema.SEP
                + Category.COL_NAME + Schema.TYPE_TEXT + Schema.SEP
                + Category.COL_LANG_KEY + Schema.TYPE_TEXT
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+Category.TABLE;
    }

    public static class Service implements BaseColumns, Schema{
        public static final String TABLE = "services";
        public static final String COL_SERVICE_ID = "service_id";
        public static final String COL_NAME = "name";
        public static final String COL_MRP = "mrp";

        public static final String SQL_CREATE = "CREATE TABLE " + Service.TABLE +" ("
                + Service.COL_SERVICE_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY + " KEY"+Schema.SEP
                + Service.COL_NAME + Schema.TYPE_TEXT + Schema.SEP
                + Service.COL_MRP + Schema.TYPE_REAL
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+Service.TABLE;
    }

    public static class SyncTime implements BaseColumns, Schema{
        public static final String TABLE = "sync_times";
        public static final String COL_TYPE = "sync_type";
        public static final String COL_TIME = "last_sync_time";

        public static final String SQL_CREATE = "CREATE TABLE "+ SyncTime.TABLE +" ("
                + SyncTime.COL_TYPE + Schema.TYPE_TEXT + Schema.INDEX_PRIMARY +" KEY"+ Schema.SEP
                + SyncTime.COL_TIME + Schema.TYPE_TEXT
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+SyncTime.TABLE;
    }

    public static class Sale implements BaseColumns, Schema{
        public static final String TABLE = "sales";
        public static final String COL_SALE_ID = "sale_id";
        public static final String COL_SERVER_SALE_ID = "server_sale_id";
        public static final String COL_MEMBER_ID = "member_id";
        public static final String COL_SERVER_MEMBER_ID = "server_member_id";
        public static final String COL_MEMBER_NAME = "member_name";
        public static final String COL_AMOUNT = "amount";
        public static final String COL_DISCOUNT = "discount";
        public static final String COL_DATE_TIME = "date_time";
        public static final String COL_STATUS = "status";
        public static final String COL_SYNC = "sync";

        public static final String SQL_CREATE = "CREATE TABLE " + Sale.TABLE +" ("
                + Sale.COL_SALE_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY + " KEY"+Schema.SEP
                + Sale.COL_SERVER_SALE_ID + Schema.TYPE_INT + Schema.UNIQUE+ Schema.SEP
                + Sale.COL_MEMBER_ID + Schema.TYPE_INT + Schema.SEP
                + Sale.COL_SERVER_MEMBER_ID + Schema.TYPE_INT + Schema.SEP
                + Sale.COL_MEMBER_NAME + Schema.TYPE_TEXT + Schema.SEP
                + Sale.COL_AMOUNT + Schema.TYPE_REAL + Schema.SEP
                + Sale.COL_DISCOUNT + Schema.TYPE_REAL + Schema.SEP
                + Sale.COL_DATE_TIME + Schema.TYPE_TEXT + Schema.SEP
                + Sale.COL_STATUS + Schema.TYPE_TEXT + Schema.SEP
                + Sale.COL_SYNC + Schema.TYPE_TEXT
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+Sale.TABLE;
    }

    public static class SaleItem implements BaseColumns, Schema{
        public static final String TABLE = "sale_items";
        public static final String COL_SALE_ITEM_ID = "sale_item_id";
        public static final String COL_SERVER_SALE_ITEM_ID = "server_sale_item_id";
        public static final String COL_SALE_ID = "sale_id";
        public static final String COL_SERVER_SALE_ID = "server_sale_id";
        public static final String COL_ITEM_TYPE = "item_type";
        public static final String COL_ITEM_ID = "item_id";
        public static final String COL_QUANTITY = "quantity";
        public static final String COL_MRP = "mrp";
        public static final String COL_PURCHASE_PRICE = "purchase_price";
        public static final String COL_DISCOUNT = "discount";
        public static final String COL_DISCOUNT_PERUNIT = "discount_perunit";
        public static final String COL_ENDUSER = "enduser";
        public static final String COL_STATUS = "status";
        public static final String COL_SYNC = "sync";

        public static final String SQL_CREATE = "CREATE TABLE " + SaleItem.TABLE +" ("
                + SaleItem.COL_SALE_ITEM_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY + " KEY"+Schema.SEP
                + SaleItem.COL_SERVER_SALE_ITEM_ID + Schema.TYPE_INT + Schema.UNIQUE + Schema.SEP
                + SaleItem.COL_SALE_ID + Schema.TYPE_INT + Schema.SEP
                + SaleItem.COL_SERVER_SALE_ID + Schema.TYPE_INT+ Schema.SEP
                + SaleItem.COL_ITEM_TYPE + Schema.TYPE_TEXT + Schema.SEP
                + SaleItem.COL_ITEM_ID + Schema.TYPE_INT + Schema.SEP
                + SaleItem.COL_QUANTITY + Schema.TYPE_INT + Schema.SEP
                + SaleItem.COL_PURCHASE_PRICE + Schema.TYPE_REAL + Schema.SEP
                + SaleItem.COL_MRP + Schema.TYPE_REAL + Schema.SEP
                + SaleItem.COL_DISCOUNT + Schema.TYPE_REAL + Schema.SEP
                + SaleItem.COL_DISCOUNT_PERUNIT + Schema.TYPE_INT + Schema.SEP
                + SaleItem.COL_ENDUSER + Schema.TYPE_TEXT + Schema.SEP
                + SaleItem.COL_STATUS + Schema.TYPE_TEXT + Schema.SEP
                + SaleItem.COL_SYNC + Schema.TYPE_TEXT
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+SaleItem.TABLE;
    }

    public static class ProductCategory implements BaseColumns, Schema{
        public static final String TABLE = "product_categories";
        public static final String COL_CATEGORY_ID = "category_id";
        public static final String COL_NAME = "name";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE +" ("
                + COL_CATEGORY_ID + Schema.TYPE_INT + Schema.INDEX_PRIMARY + " KEY"+Schema.SEP
                + COL_NAME + Schema.TYPE_TEXT
                +");";

        public static final String SQL_DROP = "DROP TABLE IF EXISTS "+TABLE;
    }

}
