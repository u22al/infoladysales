package dnet.org.bd.kallyanisales.http;

import java.util.List;

import dnet.org.bd.kallyanisales.response.GetSaleResponse;
import dnet.org.bd.kallyanisales.response.ProductCategoryResponse;
import dnet.org.bd.kallyanisales.response.ProductResponse;
import dnet.org.bd.kallyanisales.response.ServiceResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public interface GetService {
    @GET("v1/products")
    Call<List<ProductResponse>> listProducts(@Query("_token") String token, @Query("last_sync_time") String lastSyncTime);

    @GET("v1/services")
    Call<List<ServiceResponse>> listServices(@Query("_token") String token, @Query("last_sync_time") String lastSyncTime);

    @GET("v1/product-categories")
    Call<List<ProductCategoryResponse>> listProductCategories(@Query("_token") String token, @Query("last_sync_time") String lastSyncTime);

    @GET("v1/sales")
    Call<List<GetSaleResponse>> listSaleHistories(@Query("_token") String token, @Query("last_sync_time") String lastSyncTime);
}
