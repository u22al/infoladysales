package dnet.org.bd.kallyanisales.sync;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/19/2016.
 */

public interface BootCallback {
    void onSuccess();
    void onFailed();
}
