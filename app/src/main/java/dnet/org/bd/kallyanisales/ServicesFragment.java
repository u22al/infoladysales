package dnet.org.bd.kallyanisales;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dnet.org.bd.kallyanisales.adapter.ServiceAdapter;
import dnet.org.bd.kallyanisales.sync.Boot;
import dnet.org.bd.kallyanisales.sync.BootCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServicesFragment extends Fragment {

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private static boolean isServiceApiInCall=false;

    public ServicesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_services, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyler_service);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh_service_list);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Context context = getContext();
        recyclerView.setAdapter(new ServiceAdapter(context));

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isServiceApiInCall){
                    System.out.println("service in call so, return");
                    refreshLayout.setRefreshing(false);
                    return;
                }
                isServiceApiInCall = true;
                Boot.loadService(context, new BootCallback() {
                    @Override
                    public void onSuccess() {
                        isServiceApiInCall = false;
                        refreshLayout.setRefreshing(false);
                        recyclerView.swapAdapter(new ServiceAdapter(context), false);
                    }

                    @Override
                    public void onFailed() {
                        isServiceApiInCall = false;
                        refreshLayout.setRefreshing(false);
                    }
                });
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
