package dnet.org.bd.kallyanisales.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.squareup.sqlbrite.BriteDatabase;
import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.db.DbHelper;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class ProviderManager extends ContentProvider {
    public static final String CONTENT_AUTHORITY = "dnet.org.bd.kallyanisales.provider";
    public static final String PATH_PRODUCT = "products";
    public static final String PATH_SERVICE = "services";

    private static final Uri BASE_CONTENT_URI = Uri.parse("content://"+CONTENT_AUTHORITY);
    public static final Uri PRODUCT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PRODUCT).build();
    public static final Uri SERVICE_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SERVICE).build();

    public static final String PRODUCT_TYPE = "vnd.android.cursor.dir/" + PRODUCT_URI + "/" + PATH_PRODUCT;
    public static final String PRODUCT_ITEM_TYPE = "vnd.android.cursor.item/" + PRODUCT_URI + "/" + PATH_PRODUCT;
    public static final String SERVICE_TYPE = "vnd.android.cursor.dir/" + SERVICE_URI + "/" + PATH_SERVICE;
    public static final String SERVICE_ITEM_TYPE = "vnd.android.cursor.item/" + SERVICE_URI + "/" + PATH_SERVICE;

    private static final int PRODUCT = 100;
    private static final int PRODUCT_ID = 101;
    private static final int SERVICE = 200;
    private static final int SERVICE_ID = 201;

    private static UriMatcher mUriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher(){
        UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(CONTENT_AUTHORITY, PATH_PRODUCT, PRODUCT);
        matcher.addURI(CONTENT_AUTHORITY, PATH_PRODUCT+"/#", PRODUCT_ID);

        matcher.addURI(CONTENT_AUTHORITY, PATH_SERVICE, SERVICE);
        matcher.addURI(CONTENT_AUTHORITY, PATH_SERVICE+"/#", SERVICE_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] columns, String selection, String[] selectionArgs, String orderBy) {
        Cursor cursor = null;
        SQLiteDatabase db = DbHelper.getInstance(getContext()).getReadableDatabase();
        switch (mUriMatcher.match(uri)){
            case PRODUCT:
                cursor = db.query(DB.Product.TABLE, columns, selection, selectionArgs, null, null, orderBy);
                break;

            case SERVICE:
                cursor = db.query(DB.Service.TABLE, columns, selection, selectionArgs, null, null, orderBy);
                break;
        }

        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (mUriMatcher.match(uri)){
            case PRODUCT: return PRODUCT_TYPE;
            case PRODUCT_ID: return PRODUCT_ITEM_TYPE;

            case SERVICE: return SERVICE_TYPE;
            case SERVICE_ID: return SERVICE_ITEM_TYPE;

            default:
                throw new UnsupportedOperationException("Unknown uri: "+ uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        BriteDatabase db = BriteDbHelper.getBriteDatabase(getContext());
        switch (mUriMatcher.match(uri)){
            case PRODUCT:
                db.insert(DB.Product.TABLE, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                break;

            case SERVICE:
                db.insert(DB.Service.TABLE, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
                break;
        }

        return uri;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        BriteDatabase db = BriteDbHelper.getBriteDatabase(getContext());
        switch (mUriMatcher.match(uri)){
            case PRODUCT:
                return db.update(DB.Product.TABLE, contentValues, s, strings);
        }

        return 0;
    }
}
