package dnet.org.bd.kallyanisales;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import dnet.org.bd.kallyanisales.adapter.SaleItemAdapter;

public class SaleItemActivity extends AppCompatActivity {

    RecyclerView rvSaleItem;
    SwipeRefreshLayout refreshLayout;
    SaleItemAdapter adapter;
    TextView emptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_sale_item_list);
        emptyView = (TextView) findViewById(R.id.text_empty_view);
        rvSaleItem = (RecyclerView) findViewById(R.id.rv_sale_item);
        rvSaleItem.setLayoutManager(new LinearLayoutManager(this));

        TextView tvName = (TextView) findViewById(R.id.tv_customer);
        TextView tvAmount = (TextView) findViewById(R.id.tv_total);
        TextView tvDiscount = (TextView) findViewById(R.id.tv_discount);
        TextView tvDiscountAmount = (TextView) findViewById(R.id.tv_total_with_discount);
        TextView tvDate = (TextView) findViewById(R.id.tv_date);

        Intent intent = getIntent();
        int saleId = intent.getIntExtra("sale_id", 0);

        tvDate.setText(intent.getStringExtra("date"));
        tvName.setText(intent.getStringExtra("member_name"));

        float total = intent.getFloatExtra("amount", 0.0f);
        float discount = intent.getFloatExtra("discount", 0.0f);
        float grand = total-discount;
        tvDiscount.setText(discount+"");

        if(discount>0){
            tvAmount.setText("("+total+")");
            tvDiscountAmount.setText(" "+grand);
            tvAmount.setPaintFlags(tvAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            tvAmount.setTextColor(Color.RED);
        }else {
            tvDiscountAmount.setVisibility(View.GONE);
            tvAmount.setText(total + "");
        }

        adapter = new SaleItemAdapter(this, saleId);
        rvSaleItem.setAdapter(adapter);
        reloadCart();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                System.out.println("hit");
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        });


    }

    private void reloadCart(){
        if(adapter.getItemCount()<1){
            emptyView.setVisibility(View.VISIBLE);
        }else{
            emptyView.setVisibility(View.GONE);
        }
    }

}
