package dnet.org.bd.kallyanisales.response;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class AuthResponse {
    private String token;
    private String name;
    private String email;
    private int user_id;
    private String status;

    public String getToken(){
        return token;
    }

    public String getStatus(){
        return status;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getUserId() {
        return user_id;
    }
}
