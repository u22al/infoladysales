package dnet.org.bd.kallyanisales.response;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/26/2016.
 */

public class ProductCategoryResponse {
    private int category_id;
    private String name;

    public int getCategoryId(){
        return category_id;
    }

    public String getName(){
        return name;
    }
}
