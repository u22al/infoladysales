package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public class Service {
    private int serviceId;
    private String name;
    private float mrp;

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMrp() {
        return mrp;
    }

    public void setMrp(float mrp) {
        this.mrp = mrp;
    }
}
