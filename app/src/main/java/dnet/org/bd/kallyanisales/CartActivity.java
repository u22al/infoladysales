package dnet.org.bd.kallyanisales;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import dnet.org.bd.kallyanisales.adapter.CartAdapter;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.repository.CartRepository;
import dnet.org.bd.kallyanisales.sync.SyncUtils;
import dnet.org.bd.kallyanisales.util.MyContextWrapper;
import dnet.org.bd.kallyanisales.util.Tools;

public class CartActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SwipeRefreshLayout refreshLayout;
    TextView emptyView;
    TextView textGrandTotal;
    CartAdapter adapter;
    Button btnSubmit;
    Button btnMember;
    TextView tvMember;

    TextView tvTotal;
    EditText etDiscount;
    EditText etDiscountPercent;
    TextView tvGrandTotal;

    private static String member_name;
    private static String member_id;
    private static String mobile_member_id;

    private final static int REQUEST_MEMBER = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_cart);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_cart_list);
        emptyView = (TextView) findViewById(R.id.text_empty_view);
        textGrandTotal = (TextView) findViewById(R.id.text_grand_total);
        tvMember = (TextView) findViewById(R.id.tv_member_name);
        btnMember = (Button) findViewById(R.id.btn_member_load);
        btnSubmit = (Button) findViewById(R.id.btn_submit_cart);

        btnMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            selectMember();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CartAdapter(this);

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                textGrandTotal.setText(CartRepository.getGrandTotal(CartActivity.this)+"");
                System.out.println("data updated");
            }
        });

        recyclerView.setAdapter(adapter);
        reloadCart();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshLayout.setRefreshing(false);
                adapter.notifyDataSetChanged();
                reloadCart();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(member_id==null && mobile_member_id==null) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                    builder.setTitle("Please select a member first").setPositiveButton("Select", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            selectMember();
                        }
                    }).show();

                }else if(adapter.getItemCount()==0){
                    final AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                    builder.setTitle("Please add product / service").setPositiveButton("Add", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    }).show();
                }else{
                    createConfirmSellDialog();

                }
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(Tools.changeLanguage(newBase));
    }

    private void selectMember(){
        Intent memberIntent = new Intent();

        memberIntent.setComponent(new ComponentName("com.dnetinsight.ht.infolady.profiler", "com.dnetinsight.ht.infolady.profiler.activities.MemberListActivityForSales"));
        try {
            startActivityForResult(memberIntent, REQUEST_MEMBER);
        }catch (ActivityNotFoundException e){
            final AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
            builder.setTitle("Household App is not installed.").setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.cancel();
                }
            }).show();
        }
    }

    int editDiscount=0;
    private void createConfirmSellDialog(){
        View discountDialog = LayoutInflater.from(CartActivity.this).inflate(R.layout.discount_dialog, null);

        tvTotal = (TextView) discountDialog.findViewById(R.id.tv_total);
        etDiscount = (EditText) discountDialog.findViewById(R.id.et_discount);
        etDiscountPercent = (EditText) discountDialog.findViewById(R.id.et_discount_percent);
        tvGrandTotal = (TextView) discountDialog.findViewById(R.id.tv_grand_total);

        final float total = CartRepository.getGrandTotal(CartActivity.this);
        float grandTotal = total;
        tvTotal.setText(total+"");
        tvGrandTotal.setText(grandTotal+"");

        etDiscount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(editDiscount==0){
                    editDiscount = 1;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(editDiscount==2){return;}

                float dis = 0.0f;
                if(charSequence.length()>0){
                     dis = Float.parseFloat(charSequence.toString());
                }
                float percent = (dis/total)*100;
                etDiscountPercent.setText(percent + "");

                float grand = total - dis;
                if(grand<0){
                    etDiscount.setError("Discount can't be more than total");
                    etDiscount.requestFocus();
                    etDiscount.setText("");
                }
                tvGrandTotal.setText(grand+"");
                editDiscount = 0;
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        etDiscountPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(editDiscount==0){
                    editDiscount = 2;
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(editDiscount==1){return;}

                float percent = 0.0f;
                if(charSequence.length()>0){
                    percent = Float.parseFloat(charSequence.toString());
                }
                float dis = (total*percent)/100;

                etDiscount.setText(dis+"");

                float grand = total - dis;
                if(grand<0){
                    etDiscount.setError("Discount can't be more than total");
                    etDiscount.requestFocus();
                    etDiscount.setText("");
                }
                tvGrandTotal.setText(grand+"");

                editDiscount = 0;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etDiscount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    if(etDiscount.getText().toString().equals("0.0")){
                        etDiscount.setText("");
                    }
                }else{
                    if(etDiscount.getText().length()==0){
                        etDiscount.setText("0.0");
                    }
                }
            }
        });

        etDiscountPercent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b){
                    if(etDiscountPercent.getText().toString().equals("0.0")){
                        etDiscountPercent.setText("");
                    }
                }else{
                    if(etDiscountPercent.getText().length()==0){
                        etDiscountPercent.setText("0.0");
                    }
                }
            }
        });

        float itemDiscount = CartRepository.getItemsDiscount(CartActivity.this);
        AlertDialog.Builder confirmSellDialog = new AlertDialog.Builder(CartActivity.this);
        if(itemDiscount<=0) {
            confirmSellDialog.setView(discountDialog);
        }
        confirmSellDialog.setTitle("Confirm Sell")
                .setPositiveButton("Sell", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ContentValues cv = new ContentValues();
                cv.put(DB.Sale.COL_MEMBER_ID, mobile_member_id);
                cv.put(DB.Sale.COL_SERVER_MEMBER_ID, member_id);
                cv.put(DB.Sale.COL_MEMBER_NAME, member_name);

                if(etDiscount.getText().length()>0){
                    float discount = Float.parseFloat(etDiscount.getText().toString());
                    cv.put(DB.Sale.COL_DISCOUNT, discount);
                }

                member_id = mobile_member_id = member_name = null;

                CartRepository.submitCart(CartActivity.this, cv);
                recyclerView.swapAdapter(new CartAdapter(CartActivity.this), false);
                reloadCart();
                SyncUtils.TriggerRefresh();

                Intent intent = new Intent(CartActivity.this, SaleActivity.class);
                startActivity(intent);
                finish();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        tvMember.setText(member_name);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_MEMBER && resultCode==RESULT_OK){
            member_id = data.getStringExtra("id");
            mobile_member_id = data.getStringExtra("id_mobile");
            member_name = data.getStringExtra("name");
            tvMember.setText(member_name);
        }
    }

    private void reloadCart(){
        if(adapter.getItemCount()<1){
            emptyView.setVisibility(View.VISIBLE);
        }else{
            emptyView.setVisibility(View.GONE);
        }
        textGrandTotal.setText(CartRepository.getGrandTotal(this)+"");
        tvMember.setText(member_name);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_empty_cart) {

            AlertDialog.Builder emptyDialog = new AlertDialog.Builder(CartActivity.this);
            emptyDialog.setTitle("This action can't be undone, Are you sure?")
            .setPositiveButton("Empty", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    CartRepository.emptyCart(CartActivity.this);
                    recyclerView.swapAdapter(new CartAdapter(CartActivity.this), false);
                    reloadCart();
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
