package dnet.org.bd.kallyanisales.response;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class ProductResponse {
    private String name;
    private float mrp;
    private String image;
    private int category_id;
    private int product_id;
    private int brand_id;
    private float infolady_price;

    public String getName(){
        return name;
    }

    public float getMrp(){
        return mrp;
    }

    public String getImage(){
        return image;
    }

    public int getProductId(){
        return product_id;
    }

    public int getCategoryId(){
        return category_id;
    }

    public int getBrandId(){
        return brand_id;
    }

    public float getInfoladyPrice(){
        return infolady_price;
    }
}
