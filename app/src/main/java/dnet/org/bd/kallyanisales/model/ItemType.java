package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public interface ItemType {
    String PRODUCT = "product";
    String SERVICE = "service";
}
