package dnet.org.bd.kallyanisales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.List;
import dnet.org.bd.kallyanisales.R;
import dnet.org.bd.kallyanisales.http.ServerConfig;
import dnet.org.bd.kallyanisales.model.ItemType;
import dnet.org.bd.kallyanisales.model.Product;
import dnet.org.bd.kallyanisales.model.SaleItem;
import dnet.org.bd.kallyanisales.model.Service;
import dnet.org.bd.kallyanisales.repository.ProductRepository;
import dnet.org.bd.kallyanisales.repository.SaleItemRepository;
import dnet.org.bd.kallyanisales.repository.ServiceRepository;
import dnet.org.bd.kallyanisales.util.Tools;

/**
 * Created by uzzal on 12/25/2016.
 */

public class SaleItemAdapter extends RecyclerView.Adapter<SaleItemAdapter.ViewHolder>{
    public static final int PRODUCT = 0;
    public static final int SERVICE = 1;

    private static List<SaleItem> mList;
    private Context mContext;

    public SaleItemAdapter(Context context, int saleId) {
        mContext = context;
        mList = SaleItemRepository.getAll(context, saleId);
    }

    @Override
    public int getItemViewType(int position) {
        SaleItem item = mList.get(position);
        if(item.getItemType().equalsIgnoreCase(ItemType.PRODUCT)){
            return PRODUCT;
        }

        return SERVICE;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case PRODUCT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_product_item, parent, false);
                return new ProductViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sale_service_item, parent, false);
                return new ServiceViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SaleItem item = mList.get(position);

        if(holder.getItemViewType()==PRODUCT){
            ProductViewHolder h = (ProductViewHolder) holder;
            Product product = ProductRepository.getRow(mContext, item.getItemId());

            ((ProductViewHolder) holder).tvEnduser.setText(item.getEnduser());
            if(product!=null) {
                h.textName.setText(product.getName());

                String image = ServerConfig.BaseUrl+"/uploads/product/" + Tools.getImagePath(product.getImage());
                Glide.with(mContext).load(image).placeholder(R.drawable.placeholder).into(h.imageItem);
            }
        }

        if(holder.getItemViewType()==SERVICE){
            ServiceViewHolder h = (ServiceViewHolder) holder;
            Service service = ServiceRepository.getRow(mContext, item.getItemId());
            if(service!=null) {
                h.textName.setText(service.getName());
            }
        }

        holder.textMrp.setText(item.getMrp() + "");
        holder.textQuantity.setText(item.getQuantity()+"");

        float total = 0.0f;
        if(item.getPerUnit()==1) {
            total = (item.getQuantity() * item.getMrp()) - (item.getDiscount() * item.getQuantity());
            holder.textDiscount.setText((item.getDiscount() * item.getQuantity())+"");
        }else{
            total = (item.getQuantity() * item.getMrp()) - item.getDiscount();
            holder.textDiscount.setText(item.getDiscount()+"");
        }

        holder.textTotal.setText(total+"");

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ProductViewHolder extends ViewHolder{
        public ImageView imageItem;
        public TextView tvEnduser;

        public ProductViewHolder(View itemView) {
            super(itemView);

            imageItem = (ImageView) itemView.findViewById(R.id.image_item);
            tvEnduser = (TextView) itemView.findViewById(R.id.tv_enduser);
        }
    }

    public class ServiceViewHolder extends ViewHolder{
        public ServiceViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textName;
        public TextView textMrp;
        public TextView textDiscount;
        public TextView textQuantity;
        public TextView textTotal;

        public ViewHolder(View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.text_item_name);
            textMrp = (TextView) itemView.findViewById(R.id.text_item_mrp);
            textDiscount = (TextView) itemView.findViewById(R.id.text_discount);
            textQuantity = (TextView) itemView.findViewById(R.id.text_quantity);
            textTotal = (TextView) itemView.findViewById(R.id.text_total);
        }
    }
}
