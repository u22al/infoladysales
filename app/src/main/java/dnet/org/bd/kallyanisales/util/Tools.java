package dnet.org.bd.kallyanisales.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.view.animation.AnimationUtils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dnet.org.bd.kallyanisales.SettingsActivity;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class Tools {
    public static String getImagePath(String image, String prefix){
        Pattern p = Pattern.compile("([\\d]+_[\\d]+)_([\\d-\\.a-zA-Z]+)");
        Matcher m = p.matcher(image);
        if(m.find()){
            return m.group(1).replaceFirst("_","/")+"/"+prefix+m.group(2);
        }

        return null;
    }

    public static String getImagePath(String image){
        return getImagePath(image, "n");
    }

    public static boolean isInternetAvailable(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     *
     * @param context
     * @return
     */
    public static Context changeLanguage(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String lang = pref.getString(SettingsActivity.LANGUAGE, SettingsActivity.LANGUAGE_DEFAULT);

        Resources res = context.getResources();
        Configuration config = res.getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N){
            config.setLocale(locale);
        }else{
            config.locale = locale;
        }

        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN_MR1){
            return context.createConfigurationContext(config);
        }else{
            res.updateConfiguration(config, res.getDisplayMetrics());
        }

        return context;
    }
}
