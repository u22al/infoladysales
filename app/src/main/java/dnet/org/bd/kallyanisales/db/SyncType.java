package dnet.org.bd.kallyanisales.db;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public interface SyncType {
    String Product = "product";
    String Brand = "brand";
    String ProductCategory = "product_category";
    String Service = "service";
    String Sale = "sale";
    String SaleItem = "sale_item";
}
