package dnet.org.bd.kallyanisales.adapter;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;
import dnet.org.bd.kallyanisales.R;
import dnet.org.bd.kallyanisales.SaleItemActivity;
import dnet.org.bd.kallyanisales.model.Sale;
import dnet.org.bd.kallyanisales.repository.SaleRepository;

/**
 * Created by uzzal on 12/20/2016.
 */

public class SaleAdapter extends RecyclerView.Adapter<SaleAdapter.ViewHolder>{
    private static List<Sale> mList;
    private Context mContext;

    public SaleAdapter(Context context){
        mContext = context;
        mList = SaleRepository.getAll(context);
    }

    @Override
    public SaleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sale_item, parent, false);

        return new SaleAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SaleAdapter.ViewHolder holder, int position) {
        Sale sale = mList.get(position);

        holder.tvName.setText(sale.getMemberName());
        holder.tvDate.setText(sale.getDateTime());

        float discount = sale.getDiscount() + SaleRepository.getDiscount(mContext, sale.getSaleId());
        float amount = sale.getAmount();
        float grand = amount - discount;
        System.out.println("item discount: "+SaleRepository.getDiscount(mContext, sale.getSaleId()));
        System.out.println("grand discount: "+sale.getDiscount());
        System.out.println("amount: "+sale.getAmount());

        holder.tvDiscount.setText(discount+"");

        if(discount>0) {
            holder.tvTotal.setText("("+amount+")");
            holder.tvGrandTotal.setText(" " + grand);
            holder.tvTotal.setPaintFlags(holder.tvTotal.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvTotal.setTextColor(Color.RED);
        }else{
            holder.tvTotal.setText(amount+"");
            holder.tvGrandTotal.setVisibility(View.GONE);
        }
        if(sale.getSync()==null || !sale.getSync().equalsIgnoreCase("done")){
            holder.rlCard.setBackground(null);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvName;
        public TextView tvTotal;
        public TextView tvDate;
        public TextView tvDiscount;
        public TextView tvGrandTotal;
        public RelativeLayout rlCard;

        public ViewHolder(final View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvTotal = (TextView) itemView.findViewById(R.id.tv_total);
            tvDiscount = (TextView) itemView.findViewById(R.id.tv_discount);
            tvGrandTotal = (TextView) itemView.findViewById(R.id.tv_grand_total);
            rlCard = (RelativeLayout) itemView.findViewById(R.id.rl_card_content);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Sale sale = mList.get(getAdapterPosition());
                    float discount = sale.getDiscount() + SaleRepository.getDiscount(itemView.getContext(), sale.getSaleId());
                    float amount = sale.getAmount();

                    Intent intent = new Intent(itemView.getContext(), SaleItemActivity.class);
                    intent.putExtra("sale_id", sale.getSaleId());
                    intent.putExtra("member_name", sale.getMemberName());
                    intent.putExtra("amount", amount);
                    intent.putExtra("discount", discount);
                    intent.putExtra("date", sale.getDateTime());

                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }
}
