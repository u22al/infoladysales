package dnet.org.bd.kallyanisales.http;

import dnet.org.bd.kallyanisales.request.SaleRequest;
import dnet.org.bd.kallyanisales.response.PostSaleResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 1/2/2017.
 */

public interface PostService {
    @POST("v1/sales")
    Call<PostSaleResponse> sale(@Body SaleRequest sale);
}
