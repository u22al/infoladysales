package dnet.org.bd.kallyanisales.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dnet.org.bd.kallyanisales.ProductsFragment;
import dnet.org.bd.kallyanisales.ServicesFragment;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mTabCount;

    public PagerAdapter(FragmentManager fm, int tabCount){
        super(fm);
        mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new ProductsFragment();
            case 1: return new ServicesFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return mTabCount;
    }
}
