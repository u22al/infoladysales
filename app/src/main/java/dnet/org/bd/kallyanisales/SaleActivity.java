package dnet.org.bd.kallyanisales;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import dnet.org.bd.kallyanisales.adapter.SaleAdapter;

public class SaleActivity extends AppCompatActivity {

    private RecyclerView rvSale;
    private TextView tvEmptyView;
    private SaleAdapter adapter;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_sale_list);
        rvSale = (RecyclerView) findViewById(R.id.rv_sale_history);
        tvEmptyView = (TextView) findViewById(R.id.text_empty_view);

        rvSale.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SaleAdapter(this);
        rvSale.setAdapter(adapter);
        emptyPlaceholder();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
            }
        });
    }

    private void emptyPlaceholder(){
        if(adapter.getItemCount()<1){
            tvEmptyView.setVisibility(View.VISIBLE);
        }else{
            tvEmptyView.setVisibility(View.GONE);
        }
    }

}
