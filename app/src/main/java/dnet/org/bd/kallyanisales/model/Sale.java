package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/19/2016.
 */

public class Sale {
    private int saleId;
    private int serverSaleId;
    private int memberId;
    private int serverMemberId;
    private String memberName;
    private float amount;
    private float discount;
    private String dateTime;
    private String status;
    private String sync;

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public int getServerSaleId() {
        return serverSaleId;
    }

    public void setServerSaleId(int serverSaleId) {
        this.serverSaleId = serverSaleId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getServerMemberId() {
        return serverMemberId;
    }

    public void setServerMemberId(int serverMemberId) {
        this.serverMemberId = serverMemberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }
}
