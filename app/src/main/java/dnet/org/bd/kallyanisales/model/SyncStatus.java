package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/19/2016.
 */

public interface SyncStatus {
    String PENDING = "pending";
    String DONE = "done";
}
