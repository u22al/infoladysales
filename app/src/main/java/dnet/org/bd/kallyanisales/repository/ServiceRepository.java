package dnet.org.bd.kallyanisales.repository;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.squareup.sqlbrite.BriteDatabase;
import java.util.ArrayList;
import java.util.List;

import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.model.Service;
import dnet.org.bd.kallyanisales.response.ServiceResponse;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public class ServiceRepository {

    public static List<Service> getAll(Context context){
        List<Service> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+ DB.Service.TABLE + " ORDER BY "+DB.Service.COL_NAME, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Service service = new Service();
                service.setServiceId(cursor.getInt(cursor.getColumnIndex(DB.Service.COL_SERVICE_ID)));
                service.setName(cursor.getString(cursor.getColumnIndex(DB.Service.COL_NAME)));
                service.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Service.COL_MRP)));

                list.add(service);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static Service getRow(Context context, int serviceId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.Service.TABLE+" WHERE "+DB.Service.COL_SERVICE_ID+"=? LIMIT 1",new String[]{serviceId+""});
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            Service service = new Service();
            service.setName(cursor.getString(cursor.getColumnIndex(DB.Product.COL_NAME)));
            service.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_MRP)));

            cursor.close();
            return service;
        }

        return null;
    }

    public static void insert(Context context, List<ServiceResponse> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(ServiceResponse service: list){
            ContentValues cv = new ContentValues();
            cv.put(DB.Service.COL_SERVICE_ID, service.getServiceId());
            cv.put(DB.Service.COL_NAME, service.getName());
            cv.put(DB.Service.COL_MRP, service.getMrp());

            db.insert(DB.Service.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);
        }
    }
}
