package dnet.org.bd.kallyanisales;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.MenuItem;

import dnet.org.bd.kallyanisales.util.MyContextWrapper;
import dnet.org.bd.kallyanisales.util.Tools;

public class SettingsActivity extends PreferenceActivity {

    private AppCompatDelegate mDelegate;
    public static String LANGUAGE ="list_language";
    public static String LANGUAGE_DEFAULT = "en_US";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public ActionBar getSupportActionBar() {
        return getDelegate().getSupportActionBar();
    }

    private AppCompatDelegate getDelegate() {
        if (mDelegate == null) {
            mDelegate = AppCompatDelegate.create(this, null);
        }
        return mDelegate;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(Tools.changeLanguage(newBase));
    }

    public static class SettingsFragment extends PreferenceFragment{

        private Context context;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);

            System.out.println("on created");
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

            Preference notificationEnabled = findPreference(LANGUAGE);
            notificationEnabled.setSummary((pref.getString(LANGUAGE, LANGUAGE_DEFAULT).equals("en_US"))?"English (Default)":"Bengali");

            notificationEnabled.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), LANGUAGE_DEFAULT);
                    preference.setSummary((newValue.equals("en_US"))?"English (Default)":"Bengali");
                    return true;
                }
            });
        }

        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            this.context = context;
        }


        @SuppressWarnings("deprecation")
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            this.context = activity.getApplicationContext();
        }

    }
}
