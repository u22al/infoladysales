package dnet.org.bd.kallyanisales.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;

import java.util.ArrayDeque;
import java.util.List;
import dnet.org.bd.kallyanisales.R;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.http.ServerConfig;
import dnet.org.bd.kallyanisales.model.ItemType;
import dnet.org.bd.kallyanisales.model.Product;
import dnet.org.bd.kallyanisales.model.SaleItem;
import dnet.org.bd.kallyanisales.model.Service;
import dnet.org.bd.kallyanisales.repository.CartRepository;
import dnet.org.bd.kallyanisales.repository.ProductRepository;
import dnet.org.bd.kallyanisales.repository.ServiceRepository;
import dnet.org.bd.kallyanisales.util.Tools;

/**
 * Created by uzzal on 12/8/2016.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder>{
    public static final int PRODUCT = 0;
    public static final int SERVICE = 1;

    private static List<SaleItem> mList;
    private static ArrayDeque<SaleItem> stack = new ArrayDeque<>();
    private Context mContext;

    public CartAdapter(Context context){
        mContext = context;
        mList = CartRepository.getAll(context);
    }

    public void reloadAdapter(){
        mList.clear();
        mList = CartRepository.getAll(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        switch (viewType){
            case PRODUCT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_product_item, parent, false);
                return new ProductViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_service_item, parent, false);
                return new ServiceViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SaleItem item = mList.get(position);

        if(holder.getItemViewType()==PRODUCT){
            ProductViewHolder h = (ProductViewHolder) holder;
            Product product = ProductRepository.getRow(mContext, item.getItemId());
            h.textName.setText(product.getName());
            h.textMrp.setText(item.getMrp()+"");

            String image = ServerConfig.BaseUrl+"/uploads/product/"+ Tools.getImagePath(product.getImage());
            Glide.with(mContext).load(image).placeholder(R.drawable.placeholder).into(h.imageItem);
        }

        if(holder.getItemViewType()==SERVICE){
            ServiceViewHolder h = (ServiceViewHolder) holder;
            Service service = ServiceRepository.getRow(mContext, item.getItemId());
            h.textName.setText(service.getName());
            h.textMrp.setText(item.getMrp()+"");
        }

        holder.textQuantity.setText(item.getQuantity()+"");
        float total = 0.0f;
        if(item.getPerUnit()==1) {
            holder.textDiscount.setText((item.getDiscount() * item.getQuantity()) + "");
            total = (item.getQuantity() * item.getMrp()) - (item.getDiscount() * item.getQuantity());
        }else{
            holder.textDiscount.setText(item.getDiscount() + "");
            total = (item.getQuantity() * item.getMrp()) - item.getDiscount();
        }

        holder.textTotal.setText(total+"");
    }

    @Override
    public int getItemViewType(int position) {
        SaleItem item = mList.get(position);
        if(item.getItemType().equalsIgnoreCase(ItemType.PRODUCT)){
            return PRODUCT;
        }

        return SERVICE;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ProductViewHolder extends ViewHolder{
        public TextView textName;
        public TextView textMrp;
        public ImageView imageItem;
        public Spinner spEnduser;

        public ProductViewHolder(final View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.text_item_name);
            textMrp = (TextView) itemView.findViewById(R.id.text_item_mrp);
            imageItem = (ImageView) itemView.findViewById(R.id.image_item);
            spEnduser = (Spinner) itemView.findViewById(R.id.sp_enduser);

            spEnduser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    SaleItem saleItem = mList.get(getAdapterPosition());
                    String enduser = adapterView.getItemAtPosition(i).toString();
                    if(enduser.equalsIgnoreCase("...")){
                        enduser="";
                    }

                    ContentValues cv = new ContentValues();
                    cv.put(DB.SaleItem.COL_ENDUSER, enduser);

                    CartRepository.update(itemView.getContext(), cv, saleItem.getSaleItemId());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    public class ServiceViewHolder extends ViewHolder{
        public TextView textName;
        public TextView textMrp;

        public ServiceViewHolder(View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.text_item_name);
            textMrp = (TextView) itemView.findViewById(R.id.text_item_mrp);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textDiscount;
        public TextView textQuantity;
        public TextView textTotal;
        public AlertDialog.Builder removeDialogBuilder;
        public AlertDialog.Builder editDialogBuilder;
        public AlertDialog removeDialog;
        public AlertDialog editDialog;
        public ImageButton btnRemove;
        public Button btnEdit;

        public ViewHolder(final View itemView) {
            super(itemView);


            textDiscount = (TextView) itemView.findViewById(R.id.text_discount);
            textQuantity = (TextView) itemView.findViewById(R.id.text_quantity);
            textTotal = (TextView) itemView.findViewById(R.id.text_total);

            btnRemove = (ImageButton) itemView.findViewById(R.id.btn_remove_item);
            btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildRemoveDialog();
                    removeDialog.show();
                }
            });

            btnEdit = (Button) itemView.findViewById(R.id.btn_change);
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    buildEditDialog();
                    editDialog.show();
                }
            });
        }

        int discountEditing = 0;

        private void buildEditDialog(){
            editDialogBuilder = new AlertDialog.Builder(itemView.getContext());
            final View editDialogView = LayoutInflater.from(itemView.getContext()).inflate(R.layout.edit_cart_item, null);

            final SaleItem saleItem = mList.get(getAdapterPosition());
            final TextView textMrp = (TextView) editDialogView.findViewById(R.id.text_mrp);
            textMrp.setText(saleItem.getMrp()+"");

            final EditText editQuantity = (EditText) editDialogView.findViewById(R.id.edit_quantity);
            editQuantity.setText(saleItem.getQuantity()+"");

            final EditText editDiscount = (EditText) editDialogView.findViewById(R.id.edit_discount);
            editDiscount.setText(saleItem.getDiscount()+"");

            final EditText editDiscountPercent = (EditText) editDialogView.findViewById(R.id.edit_discount_percent);
            editDiscountPercent.setText(((saleItem.getDiscount()/(saleItem.getMrp() * saleItem.getQuantity()))*100)+"");

            final TextView tvTotalDiscount = (TextView) editDialogView.findViewById(R.id.tv_total_discount);
            final CheckBox cbPerunit = (CheckBox) editDialogView.findViewById(R.id.ch_discount_per_unit);

            final TextView textTotal = (TextView) editDialogView.findViewById(R.id.text_total);

            System.out.println("per unit: "+saleItem.getPerUnit());
            if(saleItem.getPerUnit()==1){
                cbPerunit.setChecked(true);
            }

            if(cbPerunit.isChecked()){
                tvTotalDiscount.setText((saleItem.getQuantity() * saleItem.getDiscount())+"");
                textTotal.setText(((saleItem.getQuantity() * saleItem.getMrp())-(saleItem.getDiscount() * saleItem.getQuantity()))+"");
            }else{
                tvTotalDiscount.setText(saleItem.getDiscount()+"");
                textTotal.setText(((saleItem.getQuantity() * saleItem.getMrp())-saleItem.getDiscount())+"");
            }

            cbPerunit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    float mrp = Float.parseFloat(textMrp.getText().toString());
                    float discount = 0.0f;
                    int quantity = 0;
                    float total=0.0f;

                    if(editDiscount.getText().length()>0){
                        try {
                            discount = Float.parseFloat(editDiscount.getText().toString());
                        }catch (NumberFormatException e){}
                    }
                    if(editQuantity.getText().length()>0) {
                        quantity = Integer.parseInt(editQuantity.getText().toString());
                    }

                    if(cbPerunit.isChecked()){
                        tvTotalDiscount.setText((quantity * discount)+"");
                        total = (mrp * quantity) - (discount * quantity);
                    }else{
                        tvTotalDiscount.setText(discount+"");
                        total = (mrp * quantity) - discount;
                    }

                    editDiscount.setText(discount+"");
                    textTotal.setText(total+"");
                }
            });

            editQuantity.addTextChangedListener(new TextWatcher() {
                float total=0.0f;
                int quantity = 0;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    float mrp = Float.parseFloat(textMrp.getText().toString());
                    float discount = 0.0f;
                    quantity = 0;
                    if(editDiscount.getText().length()>0){
                        try{
                            discount = Float.parseFloat(editDiscount.getText().toString());
                        }catch (NumberFormatException e){

                        }
                    }
                    System.out.println(quantity+"+");
                    if(charSequence.length()>0 && charSequence.charAt(0)!='-'){
                        quantity = Integer.parseInt(charSequence.toString());
                        System.out.println(quantity+",");
                    }

                    editDiscount.setText(discount+"");

                    if(cbPerunit.isChecked()){
                        tvTotalDiscount.setText((quantity * discount)+"");
                        total = (mrp * quantity) - (discount * quantity);
                    }else{
                        tvTotalDiscount.setText(discount+"");
                        total = (mrp * quantity) - discount;
                    }

                    textTotal.setText(total+"");
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(quantity<1){
                        editQuantity.setError("Quantity can't be less than 1");
                        editQuantity.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }else if(total<0){
                        editDiscount.setError("Discount can't be more than total amount");
                        editDiscount.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }else{
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                    }
                }
            });

            editDiscount.addTextChangedListener(new TextWatcher() {
                float total = 0.0f;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(discountEditing==0){
                        discountEditing = 1;
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(discountEditing==2){
                        return;
                    }

                    float mrp = Float.parseFloat(textMrp.getText().toString());
                    float discount = 0.0f;
                    int quantity=0;
                    if(charSequence.length()>0){
                        try {
                            discount = Float.parseFloat(charSequence.toString());
                        }catch (NumberFormatException e){}
                    }
                    if(editQuantity.getText().length()>0 && editQuantity.getText().charAt(0)!='-') {
                        quantity = Integer.parseInt(editQuantity.getText().toString());
                    }

                    if(cbPerunit.isChecked()){
                        total = (mrp * quantity) - (discount*quantity);
                        tvTotalDiscount.setText((discount*quantity)+"");
                    }else{
                        total = (mrp * quantity) - discount;
                        tvTotalDiscount.setText(discount+"");
                    }

                    float dis_per = (discount / (mrp * quantity))*100;
                    if(Float.isNaN(dis_per)){
                        dis_per=0.0f;
                    }

                    editDiscountPercent.setText(dis_per+"");

                    textTotal.setText(total+"");
                    discountEditing = 0;
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
                    try {
                        Float.parseFloat(editable.toString());
                    }catch (NumberFormatException e){
                        editDiscount.setError("Invalid number format");
                        editDiscount.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }

                    if(total<0){
                        editDiscount.setError("Discount can't be more than total amount");
                        editDiscount.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }
                }
            });

            editDiscountPercent.addTextChangedListener(new TextWatcher() {
                float total = 0.0f;
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(discountEditing==0){
                        discountEditing = 2;
                    }
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if(discountEditing==1){
                        return;
                    }

                    float mrp = Float.parseFloat(textMrp.getText().toString());
                    float dis_per = 0.0f;
                    int quantity=0;
                    if(charSequence.length()>0){
                        try {
                            dis_per = Float.parseFloat(charSequence.toString());
                        }catch (NumberFormatException e){}
                    }
                    if(editQuantity.getText().length()>0 && editQuantity.getText().charAt(0)!='-') {
                        quantity = Integer.parseInt(editQuantity.getText().toString());
                    }
                    
                    float amount = (mrp * quantity);
                    float discount = (amount * dis_per)/100;

                    if(cbPerunit.isChecked()){
                        total = amount - (discount * quantity);
                        tvTotalDiscount.setText((discount * quantity)+"");
                    }else{
                        total = amount - discount;
                        tvTotalDiscount.setText(discount+"");
                    }


                    editDiscount.setText(discount+"");
                    textTotal.setText(total+"");
                    discountEditing = 0;
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);

                    try {
                        Float.parseFloat(editable.toString());
                    }catch (NumberFormatException e){
                        editDiscountPercent.setError("Invalid number format");
                        editDiscountPercent.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }

                    if(total<0){
                        editDiscount.setError("Discount can't be more than total amount");
                        editDiscount.requestFocus();
                        editDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);
                    }
                }
            });

            editDiscount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        if(editDiscount.getText().toString().equals("0.0")){
                            editDiscount.setText("");
                        }
                    }else{
                        if(editDiscount.getText().length()==0){
                            editDiscount.setText("0.0");
                        }
                    }
                }
            });

            editDiscountPercent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    if(b){
                        if(editDiscountPercent.getText().toString().equals("0.0")){
                            editDiscountPercent.setText("");
                        }
                    }else{
                        if(editDiscountPercent.getText().length()==0){
                            editDiscountPercent.setText("0.0");
                        }
                    }
                }
            });

            editDialogBuilder.setView(editDialogView)
                    .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            float discount = 0.0f;
                            int quantity = 1;

                            if(editDiscount.getText().length()>0){
                                try {
                                    discount = Float.parseFloat(editDiscount.getText().toString());
                                }catch (NumberFormatException e){
                                    discount = 0.0f;
                                }
                            }

                            if(editQuantity.getText().length()>0){
                                quantity = Integer.parseInt(editQuantity.getText().toString());
                            }

                            saleItem.setDiscount(discount);
                            saleItem.setQuantity(quantity);

                            ContentValues cv = new ContentValues();
                            cv.put(DB.SaleItem.COL_QUANTITY, quantity);
                            cv.put(DB.SaleItem.COL_DISCOUNT, discount);
                            int perUnit = (cbPerunit.isChecked())?1:0;
                            System.out.println("per unit: "+perUnit);
                            cv.put(DB.SaleItem.COL_DISCOUNT_PERUNIT, perUnit);

                            CartRepository.update(itemView.getContext(), cv, saleItem.getSaleItemId());
                            System.out.println("position:" + getAdapterPosition());
                            mList.remove(getAdapterPosition());
                            mList.add(getAdapterPosition(), CartRepository.getRow(mContext, saleItem.getSaleItemId()));
                            notifyDataSetChanged();
                            CartAdapter.this.notifyDataSetChanged();
                            Toast.makeText(itemView.getContext(), "Saved", Toast.LENGTH_SHORT).show();
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });

            editDialog = editDialogBuilder.create();
        }

        private void buildRemoveDialog(){
            removeDialogBuilder = new AlertDialog.Builder(itemView.getContext());
            removeDialogBuilder.setTitle("Are you sure you want to remove this?");
            removeDialogBuilder.setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.out.println("Remove clicked "+mList.get(getAdapterPosition()).getSaleItemId());
                    final int position = getAdapterPosition();
                    stack.push(mList.get(position));
                    mList.remove(position);
                    notifyItemRemoved(position);

                    Snackbar.make(itemView, "Item removed from cart", Snackbar.LENGTH_LONG)
                            .setCallback(new Snackbar.Callback() {
                                @Override
                                public void onDismissed(Snackbar snackbar, int event) {
                                    super.onDismissed(snackbar, event);
                                    switch (event){
                                        case DISMISS_EVENT_TIMEOUT:
                                        case DISMISS_EVENT_MANUAL:
                                            for(SaleItem item: stack) {
                                                CartRepository.delete(itemView.getContext(), item.getSaleItemId());
                                                notifyDataSetChanged();
                                            }
                                            stack.clear();
                                            break;
                                    }
                                }
                            })
                            .setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mList.add(position, stack.pop());
                                    notifyDataSetChanged();
                                }
                            }).show();

                }
            });
            removeDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    System.out.println("Cancel clicked "+mList.get(getAdapterPosition()).getSaleItemId());
                }
            });

            removeDialog = removeDialogBuilder.create();
        }
    }
}
