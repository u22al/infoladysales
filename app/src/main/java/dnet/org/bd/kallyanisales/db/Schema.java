package dnet.org.bd.kallyanisales.db;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public interface Schema {
    String TYPE_TEXT = " TEXT";
    String TYPE_INT = " INTEGER";
    String TYPE_BLOB = " BLOB";
    String TYPE_REAL = " REAL";

    String INDEX_PRIMARY = " PRIMARY";
    String UNIQUE = " UNIQUE";

    String VAL_NULL = "NULL";
    String SEP = ",";
}
