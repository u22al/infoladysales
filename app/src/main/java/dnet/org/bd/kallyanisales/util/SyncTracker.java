package dnet.org.bd.kallyanisales.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite.BriteDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class SyncTracker {
    public static void updateSyncTime(Context context, String syncType){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -1);

        ContentValues cv = new ContentValues();
        cv.put(DB.SyncTime.COL_TYPE, syncType);
        cv.put(DB.SyncTime.COL_TIME, formatter.format(cal.getTime()));

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        db.insert(DB.SyncTime.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static String getLastSyncTime(Context context, String syncType){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        Cursor cursor = db.query("SELECT "+DB.SyncTime.COL_TIME+" FROM "+DB.SyncTime.TABLE + " WHERE "+DB.SyncTime.COL_TYPE+" =?", new String[]{syncType});
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex(DB.SyncTime.COL_TIME));
        }

        return null;
    }

    public static void clear(Context context){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        db.delete(DB.SyncTime.TABLE, null, null);
    }
}
