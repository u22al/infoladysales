package dnet.org.bd.kallyanisales.response;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 1/2/2017.
 */

public class PostSaleResponse {
    String status;
    Data data;

    public String getStatus(){
        return status;
    }
    public Data getData(){return data;}


    public static class Data{
        int sale_id;
        int mobile_sale_id;
        Map<Integer, Integer> sale_items = new HashMap<>();

        public int getSaleId(){
            return sale_id;
        }

        public int getMobileSaleId(){
            return mobile_sale_id;
        }

        public Map<Integer, Integer> getSaleItems(){
            return sale_items;
        }
    }
}
