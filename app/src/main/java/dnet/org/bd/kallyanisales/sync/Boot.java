package dnet.org.bd.kallyanisales.sync;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;
import java.util.Map;

import dnet.org.bd.kallyanisales.db.SyncType;
import dnet.org.bd.kallyanisales.http.PostService;
import dnet.org.bd.kallyanisales.http.GetService;
import dnet.org.bd.kallyanisales.model.Sale;
import dnet.org.bd.kallyanisales.repository.ProductCategoryRepository;
import dnet.org.bd.kallyanisales.repository.ProductRepository;
import dnet.org.bd.kallyanisales.repository.SaleItemRepository;
import dnet.org.bd.kallyanisales.repository.SaleRepository;
import dnet.org.bd.kallyanisales.repository.ServiceRepository;
import dnet.org.bd.kallyanisales.request.SaleItemRequest;
import dnet.org.bd.kallyanisales.request.SaleRequest;
import dnet.org.bd.kallyanisales.response.GetSaleResponse;
import dnet.org.bd.kallyanisales.response.ProductCategoryResponse;
import dnet.org.bd.kallyanisales.response.ProductResponse;
import dnet.org.bd.kallyanisales.response.PostSaleResponse;
import dnet.org.bd.kallyanisales.response.ServiceResponse;
import dnet.org.bd.kallyanisales.util.RetrofitClient;
import dnet.org.bd.kallyanisales.util.SyncTracker;
import dnet.org.bd.kallyanisales.util.TokenManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/19/2016.
 */

public class Boot {
    private static int loadInit=0;
    private static int loadCount=0;

    /**
     * called by sync adapter.
     * @param context
     */
    public static void loadAll(Context context){
        loadProduct(context);
        loadService(context);
        syncSale(context);
        loadProductCategories(context);
    }

    public static boolean isLoadFinished(){
        return (loadInit==loadCount);
    }

    /**
     * Called from Boot after login.
     * @param context
     * @param callback
     */
    public static void loadAll(Context context, BootCallback callback){
        loadProduct(context, callback);
        loadService(context, callback);
        loadProductCategories(context, callback);
        loadSales(context, callback);
    }

    public static void loadProduct(final Context context){
        loadProduct(context, new BootCallback() {
            @Override
            public void onSuccess() {
                System.out.println("boot product");
                loadCount++;
            }
            @Override
            public void onFailed() {}
        });
    }

    public static void loadProduct(final Context context, final BootCallback callback){
        loadInit++;
        GetService productService = RetrofitClient.getInstance().create(GetService.class);
        String token = TokenManager.getToken(context);

        String lastSyncTime = SyncTracker.getLastSyncTime(context, SyncType.Product);
        Call<List<ProductResponse>> call = productService.listProducts(token, lastSyncTime);

        call.enqueue(new Callback<List<ProductResponse>>() {
            @Override
            public void onResponse(Call<List<ProductResponse>> call, Response<List<ProductResponse>> response) {
                if(response.isSuccessful()){
                    //server
                    SyncTracker.updateSyncTime(context, SyncType.Product);
                    ProductRepository.insert(context, response.body());
                }else{
                    if(response.code()==401){
                        TokenManager.clear(context);
                    }
                }

                loadCount++;
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<List<ProductResponse>> call, Throwable t) {
                Log.e("Network Response","Failing...");
                loadCount++;
                callback.onFailed();
            }
        });
    }

    public static void loadService(final Context context){
        loadService(context, new BootCallback() {
            @Override
            public void onSuccess() {
                System.out.println("boot service");
                loadCount++;
            }

            @Override
            public void onFailed() {}
        });
    }

    public static void loadService(final Context context, final BootCallback callback){
        loadInit++;
        GetService service = RetrofitClient.getInstance().create(GetService.class);
        String token = TokenManager.getToken(context);
        String lastSyncTime = SyncTracker.getLastSyncTime(context, SyncType.Service);

        Call<List<ServiceResponse>> call = service.listServices(token, lastSyncTime);
        call.enqueue(new Callback<List<ServiceResponse>>() {
            @Override
            public void onResponse(Call<List<ServiceResponse>> call, Response<List<ServiceResponse>> response) {
                if(response.isSuccessful()) {
                    System.out.println("service success");
                    SyncTracker.updateSyncTime(context, SyncType.Service);
                    ServiceRepository.insert(context, response.body());
                }else{
                    if(response.code()==401){
                        TokenManager.clear(context);
                    }
                }

                loadCount++;
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<List<ServiceResponse>> call, Throwable t) {
                Log.e("Network Response","Failing...");
                loadCount++;
                callback.onFailed();
            }
        });
    }

    public static void loadProductCategories(Context context){
        loadProductCategories(context, new BootCallback() {
            @Override
            public void onSuccess() {
                System.out.println("product category loaded");
            }

            @Override
            public void onFailed() {

            }
        });
    }

    public static void loadProductCategories(final Context context, final BootCallback callback){
        loadInit++;
        GetService service = RetrofitClient.getInstance().create(GetService.class);
        String token = TokenManager.getToken(context);
        String lastSyncTime = SyncTracker.getLastSyncTime(context, SyncType.ProductCategory);

        Call<List<ProductCategoryResponse>> call = service.listProductCategories(token, lastSyncTime);
        call.enqueue(new Callback<List<ProductCategoryResponse>>() {
            @Override
            public void onResponse(Call<List<ProductCategoryResponse>> call, Response<List<ProductCategoryResponse>> response) {
                if(response.isSuccessful()) {
                    System.out.println("product category success");
                    SyncTracker.updateSyncTime(context, SyncType.ProductCategory);
                    ProductCategoryRepository.insert(context, response.body());
                }else{
                    if(response.code()==401){
                        TokenManager.clear(context);
                    }
                }

                loadCount++;
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<List<ProductCategoryResponse>> call, Throwable t) {
                Log.e("Network Response","Failing...");
                loadCount++;
                callback.onFailed();
            }
        });
    }

    private static int saleRequestCount = 0;
    private static int saleResponseCount = 0;

    private static boolean isSyncComplete(){
        return (saleRequestCount==saleResponseCount);
    }

    public static void syncSale(Context context){
        if(!isSyncComplete()){
            System.out.println("old sync in progress");
            return;
        }
        System.out.println("new sale sync");

        PostService service = RetrofitClient.getInstance().create(PostService.class);
        String token = TokenManager.getToken(context);
        List<Sale> list = SaleRepository.getUnsynced(context);

        saleRequestCount = list.size();
        saleResponseCount = 0;
        for (Sale sale : list){
            List<SaleItemRequest> saleItemRequests = SaleItemRepository.getAllRequest(context, sale.getSaleId());
            System.out.println(new Gson().toJson(saleItemRequests));

            SaleRequest saleRequest = new SaleRequest(token, sale.getSaleId(), sale.getServerMemberId(), sale.getAmount(), sale.getDiscount(), saleItemRequests);
            saleRequest.setMobileHouseholdMemberId(sale.getMemberId());//mobile_household_member_id
            saleRequest.setMemberName(sale.getMemberName());
            saleRequest.setCreatedAt(sale.getDateTime());
            postSale(context, service, saleRequest);
        }

    }

    private static void postSale(final Context context, PostService service, SaleRequest request){
        System.out.println("sync post sales");
        Call<PostSaleResponse> call = service.sale(request);
        call.enqueue(new Callback<PostSaleResponse>() {
            @Override
            public void onResponse(Call<PostSaleResponse> call, Response<PostSaleResponse> response) {
                System.out.println(new Gson().toJson(response));

                if(response.isSuccessful()) {
                    PostSaleResponse.Data data = response.body().getData();
                    int serverSaleId = data.getSaleId();
                    SaleRepository.updateSync(context, data.getMobileSaleId(), serverSaleId);

                    for(Map.Entry<Integer, Integer> item: data.getSaleItems().entrySet()){
                        SaleItemRepository.updateSync(context, serverSaleId, item.getKey(), item.getValue());
                    }
                }
                saleResponseCount++;
            }

            @Override
            public void onFailure(Call<PostSaleResponse> call, Throwable t) {
                saleResponseCount++;
            }
        });
    }

    public static void loadSales(Context context){
        loadSales(context, new BootCallback() {
            @Override
            public void onSuccess() {
                System.out.println("sale sync success");
            }

            @Override
            public void onFailed() {
                System.out.println("sale sync failed");
            }
        });
    }

    public static void loadSales(final Context context, final BootCallback callback){
        loadInit++;
        GetService service = RetrofitClient.getInstance().create(GetService.class);
        String token = TokenManager.getToken(context);
        String lastSyncTime = SyncTracker.getLastSyncTime(context, SyncType.Sale);

        Call<List<GetSaleResponse>> call = service.listSaleHistories(token, lastSyncTime);
        call.enqueue(new Callback<List<GetSaleResponse>>() {
            @Override
            public void onResponse(Call<List<GetSaleResponse>> call, Response<List<GetSaleResponse>> response) {
                if(response.isSuccessful()) {
                    SyncTracker.updateSyncTime(context, SyncType.Sale);
                    SaleRepository.insert(context, response.body());
                }else{
                    if(response.code()==401){
                        TokenManager.clear(context);
                    }
                }

                loadCount++;
                callback.onSuccess();
            }

            @Override
            public void onFailure(Call<List<GetSaleResponse>> call, Throwable t) {
                loadCount++;
                System.out.println("on failed");
                callback.onFailed();
            }
        });
    }
}
