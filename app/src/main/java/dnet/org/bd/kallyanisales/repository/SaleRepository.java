package dnet.org.bd.kallyanisales.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.squareup.sqlbrite.BriteDatabase;
import java.util.ArrayList;
import java.util.List;
import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.db.SyncType;
import dnet.org.bd.kallyanisales.model.Sale;
import dnet.org.bd.kallyanisales.model.SaleItem;
import dnet.org.bd.kallyanisales.model.SaleStatus;
import dnet.org.bd.kallyanisales.model.SyncStatus;
import dnet.org.bd.kallyanisales.response.GetSaleResponse;

/**
 * Created by uzzal on 12/20/2016.
 */

public class SaleRepository {

    public static List<Sale> getAll(Context context){
        List<Sale> list = new ArrayList<>();

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+ DB.Sale.TABLE+" where "+DB.Sale.COL_STATUS+"=? ORDER BY "+DB.Sale.COL_SALE_ID+" DESC", SaleStatus.SOLD);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Sale sale = new Sale();

                sale.setSaleId(cursor.getInt(cursor.getColumnIndex(DB.Sale.COL_SALE_ID)));
                sale.setMemberName(cursor.getString(cursor.getColumnIndex(DB.Sale.COL_MEMBER_NAME)));
                sale.setAmount(cursor.getFloat(cursor.getColumnIndex(DB.Sale.COL_AMOUNT)));
                sale.setDateTime(cursor.getString(cursor.getColumnIndex(DB.Sale.COL_DATE_TIME)));
                sale.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.Sale.COL_DISCOUNT)));
                sale.setSync(cursor.getString(cursor.getColumnIndex(DB.Sale.COL_SYNC)));

                list.add(sale);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<Sale> getAll(Context context, String condition, String... args){
        List<Sale> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.Sale.TABLE+ " where "+condition, args);
        System.out.println("select * from "+DB.Sale.TABLE+" where "+condition);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Sale sale = new Sale();
                sale.setSaleId(cursor.getInt(cursor.getColumnIndex(DB.Sale.COL_SALE_ID)));
                sale.setMemberName(cursor.getString(cursor.getColumnIndex(DB.Sale.COL_MEMBER_NAME)));
                sale.setMemberId(cursor.getInt(cursor.getColumnIndex(DB.Sale.COL_MEMBER_ID)));
                sale.setServerMemberId(cursor.getInt(cursor.getColumnIndex(DB.Sale.COL_SERVER_MEMBER_ID)));
                sale.setAmount(cursor.getFloat(cursor.getColumnIndex(DB.Sale.COL_AMOUNT)));
                sale.setDateTime(cursor.getString(cursor.getColumnIndex(DB.Sale.COL_DATE_TIME)));
                sale.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.Sale.COL_DISCOUNT)));

                list.add(sale);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<Sale> getUnsynced(Context context){
        return getAll(context, DB.Sale.COL_SYNC+" IS NULL ", null);
    }

    public static int updateSync(Context context, int saleId, int serverSaleId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        ContentValues cv = new ContentValues();
        cv.put(DB.Sale.COL_SERVER_SALE_ID, serverSaleId);
        cv.put(DB.Sale.COL_SYNC, "done");

        return db.update(DB.Sale.TABLE, cv, DB.Sale.COL_SALE_ID+"=?",new String[]{saleId+""});
    }

    public static void delete(Context context,@Nullable String whereClause, @Nullable String... whereArgs){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        db.delete(DB.Sale.TABLE, null, null);
    }

    public static void insert(Context context, List<GetSaleResponse> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(GetSaleResponse item: list){
            BriteDatabase.Transaction transaction = db.newTransaction();
            try {
                ContentValues cv = new ContentValues();

                cv.put(DB.Sale.COL_SERVER_SALE_ID, item.getSaleId());
                cv.put(DB.Sale.COL_SERVER_MEMBER_ID, item.getHouseholdMemberId());
                cv.put(DB.Sale.COL_MEMBER_ID, item.getMobileHouseholdMemberId());
                cv.put(DB.Sale.COL_SERVER_MEMBER_ID, item.getHouseholdMemberId());
                cv.put(DB.Sale.COL_DATE_TIME, item.getCreatedAt());
                cv.put(DB.Sale.COL_AMOUNT, item.getAmount());
                cv.put(DB.Sale.COL_MEMBER_NAME, item.getMemberName());
                cv.put(DB.Sale.COL_DISCOUNT, item.getDiscount());
                cv.put(DB.Sale.COL_STATUS, SaleStatus.SOLD);
                cv.put(DB.Sale.COL_SYNC, SyncStatus.DONE);
                long saleId = db.insert(DB.Sale.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

                for (GetSaleResponse.Items saleItem : item.getItems()) {
                    ContentValues cv2 = new ContentValues();
                    cv2.put(DB.SaleItem.COL_SERVER_SALE_ITEM_ID, saleItem.getSaleItemId());
                    cv2.put(DB.SaleItem.COL_SERVER_SALE_ID, saleItem.getSaleId());
                    cv2.put(DB.SaleItem.COL_SALE_ID, saleId);
                    cv2.put(DB.SaleItem.COL_ITEM_TYPE, saleItem.getItemType());
                    cv2.put(DB.SaleItem.COL_ITEM_ID, saleItem.getItemId());
                    cv2.put(DB.SaleItem.COL_QUANTITY, saleItem.getQuantity());
                    cv2.put(DB.SaleItem.COL_MRP, saleItem.getMrp());
                    cv2.put(DB.SaleItem.COL_PURCHASE_PRICE, saleItem.getHubPrice());
                    cv2.put(DB.SaleItem.COL_DISCOUNT, saleItem.getDiscount());
                    cv2.put(DB.SaleItem.COL_DISCOUNT_PERUNIT, saleItem.getDiscountPerunit());
                    cv2.put(DB.SaleItem.COL_ENDUSER, saleItem.getEnduser());
                    cv2.put(DB.SaleItem.COL_STATUS, SaleStatus.SOLD);
                    cv2.put(DB.SaleItem.COL_SYNC, SyncStatus.DONE);
                    db.insert(DB.SaleItem.TABLE, cv2, SQLiteDatabase.CONFLICT_REPLACE);
                }

                transaction.markSuccessful();
            }finally {
                transaction.end();
            }
        }
    }

    public static float getDiscount(Context context, int saleId){
        List<SaleItem> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("SELECT * FROM "+DB.SaleItem.TABLE+" WHERE "+DB.Sale.COL_SALE_ID+"=?", saleId+"");
        float total = 0.0f;
        if(cursor!=null && cursor.moveToFirst()){
            do{
                int isPerunit = cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT_PERUNIT));
                float discount = cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT));
                int quantity = cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY));

                if(isPerunit==1){
                    total += discount * quantity;
                }else {
                    total += discount;
                }

            }while (cursor.moveToNext());
            cursor.close();
        }

        return total;
    }


}
