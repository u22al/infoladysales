package dnet.org.bd.kallyanisales.response;

import java.util.List;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 1/5/2017.
 */

public class GetSaleResponse {
    int sale_id;
    int household_member_id;
    int mobile_household_member_id;
    int user_id;
    float amount;
    float discount;
    String member_name;
    String status;
    String created_at;
    List<Items> items;

    public int getSaleId() {
        return sale_id;
    }

    public int getHouseholdMemberId() {
        return household_member_id;
    }

    public int getMobileHouseholdMemberId() {
        return mobile_household_member_id;
    }

    public int getUserId() {
        return user_id;
    }

    public float getDiscount() {
        return discount;
    }

    public String getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public List<Items> getItems() {
        return items;
    }

    public String getMemberName() {
        return member_name;
    }

    public float getAmount() {
        return amount;
    }

    public static class Items{
        int sale_item_id;
        int sale_id;
        String item_type;
        int item_id;
        int quantity;
        float hub_price;
        float infolady_price;
        float mrp;
        float discount;
        int discount_perunit;
        String enduser;
        String status;

        public int getSaleItemId() {
            return sale_item_id;
        }

        public String getEnduser() {
            return enduser;
        }

        public String getStatus() {
            return status;
        }

        public int getDiscountPerunit() {
            return discount_perunit;
        }

        public float getDiscount() {
            return discount;
        }

        public float getMrp() {
            return mrp;
        }

        public float getInfoladyPrice() {
            return infolady_price;
        }

        public float getHubPrice() {
            return hub_price;
        }

        public int getQuantity() {
            return quantity;
        }

        public int getItemId() {
            return item_id;
        }

        public String getItemType() {
            return item_type;
        }

        public int getSaleId() {
            return sale_id;
        }
    }

}
