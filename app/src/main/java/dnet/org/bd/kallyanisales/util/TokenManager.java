package dnet.org.bd.kallyanisales.util;

import android.content.Context;
import android.content.SharedPreferences;

import dnet.org.bd.kallyanisales.repository.SaleItemRepository;
import dnet.org.bd.kallyanisales.repository.SaleRepository;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class TokenManager {
    final private static String TOKEN = "_token";
    final private static String EMAIL = "email";
    final private static String NAME = "name";

    public static void setToken(Context context, String token){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public static void clear(Context context){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        pref.edit().remove(TOKEN).commit();
    }

    public static String getToken(Context context){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        return pref.getString(TOKEN, null);
    }

    public static void setToken(Context context, String token, String name, String email){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(TOKEN, token);
        editor.putString(EMAIL, email);
        editor.putString(NAME, name);
        editor.commit();
    }

    public static String getName(Context context){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        return pref.getString(NAME, "unknown");
    }

    public static String getEmail(Context context){
        SharedPreferences pref = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
        return pref.getString(EMAIL, "unknown");
    }

    public static void logout(Context context){
        clear(context);
        SaleRepository.delete(context, null, null);
        SaleItemRepository.delete(context, null, null);
        SyncTracker.clear(context);
    }
}
