package dnet.org.bd.kallyanisales.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class DbHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "kallyani_sales.db";
    private static int VERSION = 1;
    private static DbHelper sInstance;

    private DbHelper(Context context){
        super(context, DB_NAME, null, VERSION);
    }

    public static synchronized DbHelper getInstance(Context context){
        if(sInstance == null){
            sInstance = new DbHelper(context);
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DB.Product.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Category.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.SyncTime.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Service.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Sale.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.SaleItem.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.ProductCategory.SQL_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DB.Product.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Category.SQL_DROP);
        sqLiteDatabase.execSQL(DB.SyncTime.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Service.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Sale.SQL_DROP);
        sqLiteDatabase.execSQL(DB.SaleItem.SQL_DROP);
        sqLiteDatabase.execSQL(DB.ProductCategory.SQL_DROP);
        onCreate(sqLiteDatabase);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
