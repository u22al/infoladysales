package dnet.org.bd.kallyanisales.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.Nullable;

import com.squareup.sqlbrite.BriteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.model.SaleItem;
import dnet.org.bd.kallyanisales.model.SaleStatus;
import dnet.org.bd.kallyanisales.request.SaleItemRequest;

/**
 * Created by uzzal on 12/25/2016.
 */

public class SaleItemRepository {

    public static List<SaleItem> getAll(Context context, int saleId){
        List<SaleItem> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+ DB.SaleItem.TABLE+" where "+DB.SaleItem.COL_SALE_ID+"=? AND "+DB.SaleItem.COL_STATUS+"=?", saleId+"", SaleStatus.SOLD);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                SaleItem item = new SaleItem();
                item.setSaleItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_SALE_ITEM_ID)));
                item.setItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_ID)));
                item.setQuantity(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY)));
                item.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_MRP)));
                item.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT)));
                item.setItemType(cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_TYPE)));
                item.setPerUnit(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT_PERUNIT)));
                item.setEnduser(cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ENDUSER)));

                list.add(item);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<SaleItemRequest> getAllRequest(Context context, int saleId){
        List<SaleItemRequest> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+ DB.SaleItem.TABLE+" where "+DB.SaleItem.COL_SALE_ID+"=? AND "
                +DB.SaleItem.COL_STATUS+"=? AND "+DB.SaleItem.COL_SYNC+" IS NULL", saleId+"", SaleStatus.SOLD);

        if(cursor!=null && cursor.moveToFirst()){
            do{
                SaleItemRequest item = new SaleItemRequest();

                item.setSaleItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_SALE_ITEM_ID)));
                item.setItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_ID)));
                item.setItemType(cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_TYPE)));
                item.setQuantity(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY)));
                item.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_MRP)));
                item.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT)));
                item.setInfoladyPrice(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_PURCHASE_PRICE)));
                item.setDiscountPerunit(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT_PERUNIT)));
                String enduser = cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ENDUSER));
                item.setEnduser((enduser==null)?"":enduser);

                list.add(item);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static int updateSync(Context context, int serverSaleId,  int saleItemId, int serverSaleItemId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        ContentValues cv = new ContentValues();
        cv.put(DB.SaleItem.COL_SERVER_SALE_ID, serverSaleId);
        cv.put(DB.SaleItem.COL_SERVER_SALE_ITEM_ID, serverSaleItemId);
        cv.put(DB.SaleItem.COL_SYNC, "done");

        return db.update(DB.SaleItem.TABLE, cv, DB.SaleItem.COL_SALE_ITEM_ID+"=?",new String[]{saleItemId+""});
    }

    public static void delete(Context context, @Nullable String whereClause, @Nullable String... whereArgs){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        db.delete(DB.SaleItem.TABLE, null, null);
    }
}
