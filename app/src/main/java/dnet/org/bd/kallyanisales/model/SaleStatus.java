package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public interface SaleStatus {
    String PENDING = "pending";
    String SOLD = "sold";
}
