package dnet.org.bd.kallyanisales.response;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public class ServiceResponse {
    private int service_id;
    private String name;
    private float mrp;

    public int getServiceId() {
        return service_id;
    }

    public String getName() {
        return name;
    }

    public float getMrp() {
        return mrp;
    }
}
