package dnet.org.bd.kallyanisales.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.db.DbHelper;
import dnet.org.bd.kallyanisales.model.ProductCategory;
import dnet.org.bd.kallyanisales.response.ProductCategoryResponse;


/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/26/2016.
 */

public class ProductCategoryRepository {
    private static List<String> list = new ArrayList<>();

    public static List<String> getAll(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getReadableDatabase();
        list.clear();

        list.add("All");
        Cursor cursor = db.rawQuery("select * from "+ DB.ProductCategory.TABLE+" ORDER BY "+DB.ProductCategory.COL_NAME, new String[]{});
        if(cursor!=null && cursor.moveToFirst()){
            do{

                System.out.println("category: "+cursor.getString(cursor.getColumnIndex(DB.ProductCategory.COL_NAME)));
                list.add(cursor.getString(cursor.getColumnIndex(DB.ProductCategory.COL_NAME)));

            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static int getId(Context context, String name){
        SQLiteDatabase db = DbHelper.getInstance(context).getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from "+ DB.ProductCategory.TABLE+" where name=? LIMIT 1", new String[]{name});
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            return cursor.getInt(cursor.getColumnIndex(DB.ProductCategory.COL_CATEGORY_ID));
        }

        return 0;
    }

    public static void insert(Context context, List<ProductCategoryResponse> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(ProductCategoryResponse item: list){
            ContentValues cv = new ContentValues();
            cv.put(DB.ProductCategory.COL_CATEGORY_ID, item.getCategoryId());
            cv.put(DB.ProductCategory.COL_NAME, item.getName());

            db.insert(DB.ProductCategory.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);
        }
    }
}
