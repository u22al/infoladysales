package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/6/2016.
 */

public class Product {
    private int productId;
    private String name;
    private float mrp;
    private float infoladyPrice;
    private String image;

    public String getName() {
        return name;
    }

    public float getMrp() {
        return mrp;
    }

    public float getInfoladyPrice(){
        return infoladyPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getImage() {
        return image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMrp(float mrp) {
        this.mrp = mrp;
    }

    public void setInfoladyPrice(float infoladyPrice) {
        this.infoladyPrice = infoladyPrice;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
