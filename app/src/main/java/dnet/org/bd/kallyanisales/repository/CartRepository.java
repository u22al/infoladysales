package dnet.org.bd.kallyanisales.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.squareup.sqlbrite.BriteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import dnet.org.bd.kallyanisales.CartActivity;
import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.model.SaleItem;
import dnet.org.bd.kallyanisales.model.SaleStatus;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public class CartRepository {
    public static long add(Context context, ContentValues cv){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        cv.put(DB.SaleItem.COL_STATUS, SaleStatus.PENDING);

        String type=cv.getAsString(DB.SaleItem.COL_ITEM_TYPE);
        int id = cv.getAsInteger(DB.SaleItem.COL_ITEM_ID);

        String cond = DB.SaleItem.COL_ITEM_TYPE+"=? AND "+DB.SaleItem.COL_ITEM_ID+"=? AND "+DB.SaleItem.COL_STATUS+"=?";
        String sql = "select * from "+DB.SaleItem.TABLE+" where "+cond;
        String[] values = {type, id+"", SaleStatus.PENDING};

        Cursor cursor = db.query(sql, values);

        cursor.moveToFirst();
        if(cursor.getCount()>0) {
            int quantity = cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY));
            quantity += cv.getAsInteger(DB.SaleItem.COL_QUANTITY);
            System.out.println("quantity: "+quantity);

            cv.put(DB.SaleItem.COL_QUANTITY, quantity);
            db.update(DB.SaleItem.TABLE, cv, cond, values);
            cursor.close();

            return id;
        }else{
            return db.insert(DB.SaleItem.TABLE, cv);
        }
    }

    public static int emptyCart(Context context){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        return db.delete(DB.SaleItem.TABLE, DB.SaleItem.COL_STATUS+"=?", new String[]{SaleStatus.PENDING});
    }

    public static List<SaleItem> getAll(Context context){
        List<SaleItem> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.SaleItem.TABLE+" where "+DB.SaleItem.COL_STATUS+"=?", SaleStatus.PENDING);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                SaleItem item = new SaleItem();
                item.setSaleItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_SALE_ITEM_ID)));
                item.setItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_ID)));
                item.setQuantity(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY)));
                item.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_MRP)));
                item.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT)));
                item.setItemType(cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_TYPE)));
                item.setPerUnit(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT_PERUNIT)));

                list.add(item);
            }while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static SaleItem getRow(Context context, int saleItemID){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.SaleItem.TABLE+" where "+DB.SaleItem.COL_SALE_ITEM_ID+"=? limit 1", saleItemID+"");
        SaleItem item = new SaleItem();
        if(cursor !=null && cursor.moveToNext()){
            item.setSaleItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_SALE_ITEM_ID)));
            item.setItemId(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_ID)));
            item.setQuantity(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_QUANTITY)));
            item.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_MRP)));
            item.setDiscount(cursor.getFloat(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT)));
            item.setItemType(cursor.getString(cursor.getColumnIndex(DB.SaleItem.COL_ITEM_TYPE)));
            item.setPerUnit(cursor.getInt(cursor.getColumnIndex(DB.SaleItem.COL_DISCOUNT_PERUNIT)));
        }
        return item;
    }

    public static float getAmount(Context context){
        List<SaleItem> list = getAll(context);
        float total = 0.0f;
        for(SaleItem item: list){
                total += item.getMrp() * item.getQuantity();
        }

        return total;
    }

    public static float getGrandTotal(Context context){
        List<SaleItem> list = getAll(context);
        float total = 0.0f;
        for(SaleItem item: list){
            if(item.getPerUnit()==1){
                total += (item.getMrp() * item.getQuantity()) - (item.getDiscount() * item.getQuantity());
            }else {
                total += (item.getMrp() * item.getQuantity()) - item.getDiscount();
            }
        }

        return total;
    }

    public static float getItemsDiscount(Context context){
        List<SaleItem> list = getAll(context);
        float total = 0.0f;
        for(SaleItem item: list){
            total += item.getDiscount();
        }

        return total;
    }

    public static int update(Context context, ContentValues cv, int saleItemId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        return db.update(DB.SaleItem.TABLE, cv, DB.SaleItem.COL_SALE_ITEM_ID+"=?",new String[]{saleItemId+""});
    }

    public static int update(Context context, ContentValues cv, String where, String[] args){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        return db.update(DB.SaleItem.TABLE, cv, where, args);
    }

    public static int delete(Context context, int saleItemId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        return db.delete(DB.SaleItem.TABLE, DB.SaleItem.COL_SALE_ITEM_ID+"=?",new String[]{saleItemId+""});
    }

    public static void submitCart(Context context, ContentValues cv){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        BriteDatabase.Transaction transaction = db.newTransaction();
        try{
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTime(new Date());

            cv.put(DB.Sale.COL_DATE_TIME, formatter.format(cal.getTime()));
            //cv.put(DB.Sale.COL_AMOUNT, CartRepository.getGrandTotal(context));
            cv.put(DB.Sale.COL_AMOUNT, CartRepository.getAmount(context));
            cv.put(DB.Sale.COL_STATUS, SaleStatus.SOLD);

            long saleId = db.insert(DB.Sale.TABLE, cv);
            ContentValues row = new ContentValues();
            row.put(DB.SaleItem.COL_STATUS, SaleStatus.SOLD);
            row.put(DB.SaleItem.COL_SALE_ID, saleId);
            update(context, row, DB.SaleItem.COL_STATUS+"=?", new String[]{SaleStatus.PENDING});

            transaction.markSuccessful();
        }finally {
            transaction.end();
        }
    }
}