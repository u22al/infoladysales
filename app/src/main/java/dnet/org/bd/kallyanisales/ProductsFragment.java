package dnet.org.bd.kallyanisales;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.List;
import dnet.org.bd.kallyanisales.adapter.ProductAdapter;
import dnet.org.bd.kallyanisales.model.Product;
import dnet.org.bd.kallyanisales.repository.ProductCategoryRepository;
import dnet.org.bd.kallyanisales.repository.ProductRepository;
import dnet.org.bd.kallyanisales.sync.Boot;
import dnet.org.bd.kallyanisales.sync.BootCallback;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class ProductsFragment extends Fragment {

    private SwipeRefreshLayout refreshProduct;
    private RecyclerView recyclerProduct;
    private ProductAdapter productAdapter;
    private static boolean isProductApiInCall=false;
    Spinner spProductCategories;

    public ProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        List<String>  categoryList = ProductCategoryRepository.getAll(getContext());
        ArrayAdapter<String> spAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, categoryList);

        spProductCategories = (Spinner) view.findViewById(R.id.sp_product_categories);
        spProductCategories.setAdapter(spAdapter);

        recyclerProduct = (RecyclerView) view.findViewById(R.id.recycler_product);
        recyclerProduct.setLayoutManager(new LinearLayoutManager(getContext()));

        refreshProduct = (SwipeRefreshLayout) view.findViewById(R.id.refresh_product_list);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Context context = getContext();
        Log.d("ProductFragment.java", "onViewCreated");
        productAdapter = new ProductAdapter(context);
        System.out.println("adapter count: "+productAdapter.getItemCount());
        recyclerProduct.setAdapter(productAdapter);

        refreshProduct.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if(isProductApiInCall){
                    System.out.println("returned");
                    refreshProduct.setRefreshing(false);
                    return;
                }
                isProductApiInCall = true;

                Boot.loadProduct(context, new BootCallback() {
                    @Override
                    public void onSuccess() {
                        System.out.println("boot success callback");
                        isProductApiInCall = false;
                        refreshProduct.setRefreshing(false);

                        recyclerProduct.swapAdapter(new ProductAdapter(context), false);
                    }

                    @Override
                    public void onFailed() {
                        System.out.println("boot failed callback");
                        isProductApiInCall = false;
                        refreshProduct.setRefreshing(false);
                    }
                });
            }
        });

        spProductCategories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println("position: "+i);
                String category = adapterView.getItemAtPosition(i).toString();
                System.out.println(category);
                int categoryId = ProductCategoryRepository.getId(getContext(), category);
                if(categoryId>0){
                    List<Product> list =ProductRepository.getAll(context, categoryId);
                    recyclerProduct.swapAdapter(new ProductAdapter(context, list), false);
                }else{
                    recyclerProduct.swapAdapter(new ProductAdapter(context), false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
