package dnet.org.bd.kallyanisales.request;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 1/2/2017.
 */

public class SaleItemRequest {
    private int sale_item_id;
    private String item_type;
    private int item_id;
    private int quantity;
    private float hub_price;
    private float infolady_price;
    private float mrp;
    private float discount;
    private String enduser;
    private int discount_perunit;

    public SaleItemRequest(){}
    public SaleItemRequest(int saleItemId, String itemType, int itemId, int quantity){
        sale_item_id = saleItemId;
        item_type = itemType;
        item_id = itemId;
        this.quantity = quantity;
    }

    public void setSaleItemId(int sale_item_id) {
        this.sale_item_id = sale_item_id;
    }

    public void setItemType(String item_type) {
        this.item_type = item_type;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setItemId(int item_id) {
        this.item_id = item_id;
    }

    public void setHubPrice(float hub_price) {
        this.hub_price = hub_price;
    }

    public void setInfoladyPrice(float infolady_price) {
        this.infolady_price = infolady_price;
    }

    public void setMrp(float mrp) {
        this.mrp = mrp;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public void setDiscountPerunit(int discount_perunit) {
        this.discount_perunit = discount_perunit;
    }

    public void setEnduser(String enduser) {
        this.enduser = enduser;
    }
}
