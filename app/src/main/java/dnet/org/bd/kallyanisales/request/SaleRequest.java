package dnet.org.bd.kallyanisales.request;

import java.util.List;


/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 1/2/2017.
 */

public class SaleRequest {
    private String _token;
    private int household_member_id;
    private int mobile_household_member_id;
    private String member_name;
    private float amount;
    private float discount;
    private int sale_id;
    private String created_at;
    private List<SaleItemRequest> items;

    public SaleRequest(String token, int saleId, int householdMemberId, float amount, float discount, List<SaleItemRequest> items){
        _token = token;
        sale_id = saleId;
        household_member_id = householdMemberId;
        this.discount = discount;
        this.amount = amount;
        this.items = items;
    }

    public void setMemberName(String name){
        member_name = name;
    }

    public void setMobileHouseholdMemberId(int mobile_household_member_id) {
        this.mobile_household_member_id = mobile_household_member_id;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }
}
