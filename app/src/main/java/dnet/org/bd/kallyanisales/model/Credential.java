package dnet.org.bd.kallyanisales.model;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class Credential {
    private String email;
    private String password;

    public Credential(String email, String password){
        this.email = email;
        this.password = password;
    }
}
