package dnet.org.bd.kallyanisales.repository;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite.BriteContentResolver;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.ArrayList;
import java.util.List;

import dnet.org.bd.kallyanisales.db.BriteDbHelper;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.db.DbHelper;
import dnet.org.bd.kallyanisales.model.Product;
import dnet.org.bd.kallyanisales.provider.ProviderManager;
import dnet.org.bd.kallyanisales.response.ProductResponse;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class ProductRepository {

    private static List<Product> list = new ArrayList<>();
    private static Action1<SqlBrite.Query> queryAction1 = new Action1<SqlBrite.Query>() {
        @Override
        public void call(SqlBrite.Query query) {
            Cursor cursor = query.run();
            list.clear();
            if(cursor!=null && cursor.moveToFirst()){
                do{
                    Product product = new Product();
                    product.setName(cursor.getString(cursor.getColumnIndex(DB.Product.COL_NAME)));
                    product.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_MRP)));
                    product.setImage(cursor.getString(cursor.getColumnIndex(DB.Product.COL_IMAGE)));
                    product.setInfoladyPrice(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_INFOLADY_PRICE)));
                    product.setImage(cursor.getString(cursor.getColumnIndex(DB.Product.COL_IMAGE)));

                    list.add(product);
                }while(cursor.moveToNext());
                cursor.close();
            }
        }
    };

    public static List<Product> getAll(ContentResolver resolver){
        String[] projection = {
                DB.Product.COL_NAME,
                DB.Product.COL_MRP,
                DB.Product.COL_IMAGE,
                DB.Product.COL_INFOLADY_PRICE,
                DB.Product.COL_IMAGE
        };

        BriteContentResolver briteContentResolver = BriteDbHelper.getBriteResolver(resolver);

        Observable<SqlBrite.Query> query = briteContentResolver.createQuery(ProviderManager.PRODUCT_URI, projection, null, null, null, false);
        query.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(queryAction1);

        return list;
    }

    public static List<Product> getAll(Context context){
        SQLiteDatabase db = DbHelper.getInstance(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.Product.TABLE + " ORDER BY "+DB.Product.COL_NAME, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Product product = new Product();
                product.setProductId(cursor.getInt(cursor.getColumnIndex(DB.Product.COL_PRODUCT_ID)));
                product.setName(cursor.getString(cursor.getColumnIndex(DB.Product.COL_NAME)));
                product.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_MRP)));
                product.setInfoladyPrice(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_INFOLADY_PRICE)));
                product.setImage(cursor.getString(cursor.getColumnIndex(DB.Product.COL_IMAGE)));

                list.add(product);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<Product> getAll(Context context, int CategoryId){
        SQLiteDatabase db = DbHelper.getInstance(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.Product.TABLE+" where "+DB.Product.COL_CATEGORY_ID+"=? ORDER BY "+DB.Product.COL_NAME, new String[]{CategoryId+""});
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Product product = new Product();
                product.setProductId(cursor.getInt(cursor.getColumnIndex(DB.Product.COL_PRODUCT_ID)));
                product.setName(cursor.getString(cursor.getColumnIndex(DB.Product.COL_NAME)));
                product.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_MRP)));
                product.setInfoladyPrice(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_INFOLADY_PRICE)));
                product.setImage(cursor.getString(cursor.getColumnIndex(DB.Product.COL_IMAGE)));

                list.add(product);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static Product getRow(Context context, int productId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.Product.TABLE+" WHERE "+DB.Product.COL_PRODUCT_ID+"=? LIMIT 1",new String[]{productId+""});
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            Product product = new Product();
            product.setName(cursor.getString(cursor.getColumnIndex(DB.Product.COL_NAME)));
            product.setMrp(cursor.getFloat(cursor.getColumnIndex(DB.Product.COL_MRP)));
            product.setImage(cursor.getString(cursor.getColumnIndex(DB.Product.COL_IMAGE)));
            cursor.close();

            return product;
        }

        return null;
    }

    public static void insert(Context context, List<ProductResponse> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(ProductResponse product: list){
            ContentValues cv = new ContentValues();
            cv.put(DB.Product.COL_PRODUCT_ID, product.getProductId());
            cv.put(DB.Product.COL_CATEGORY_ID, product.getCategoryId());
            cv.put(DB.Product.COL_BRAND_ID, product.getBrandId());
            cv.put(DB.Product.COL_NAME, product.getName());
            cv.put(DB.Product.COL_IMAGE, product.getImage());
            cv.put(DB.Product.COL_MRP, product.getMrp());
            cv.put(DB.Product.COL_INFOLADY_PRICE, product.getInfoladyPrice());

            db.insert(DB.Product.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);
        }
        context.getContentResolver().notifyChange(ProviderManager.PRODUCT_URI, null);
    }

    public static void insert(ContentResolver resolver, List<ProductResponse> list){
        ContentValues[] cvArr = new ContentValues[list.size()];
        int i=0;
        for(ProductResponse product: list){
            ContentValues cv = new ContentValues();
            cv.put(DB.Product.COL_PRODUCT_ID, product.getProductId());
            cv.put(DB.Product.COL_CATEGORY_ID, product.getCategoryId());
            cv.put(DB.Product.COL_BRAND_ID, product.getBrandId());
            cv.put(DB.Product.COL_NAME, product.getName());
            cv.put(DB.Product.COL_IMAGE, product.getImage());
            cv.put(DB.Product.COL_MRP, product.getMrp());
            cv.put(DB.Product.COL_INFOLADY_PRICE, product.getInfoladyPrice());

            cvArr[i] = cv;
            i++;

        }

        resolver.bulkInsert(ProviderManager.PRODUCT_URI, cvArr);
        resolver.notifyChange(ProviderManager.PRODUCT_URI, null);
    }
}
