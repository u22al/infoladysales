package dnet.org.bd.kallyanisales.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import dnet.org.bd.kallyanisales.R;
import dnet.org.bd.kallyanisales.db.DB;
import dnet.org.bd.kallyanisales.model.ItemType;
import dnet.org.bd.kallyanisales.model.Service;
import dnet.org.bd.kallyanisales.repository.CartRepository;
import dnet.org.bd.kallyanisales.repository.ServiceRepository;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/8/2016.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder>{
    private static List<Service> mList;
    private static Toast toast;
    private Context mContext;

    public ServiceAdapter(Context context){
        mContext = context;
        mList = ServiceRepository.getAll(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View serviceList = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_item, parent, false);
        return new ViewHolder(serviceList);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Service service = mList.get(position);

        holder.textName.setText(service.getName());
        holder.textMrp.setText(service.getMrp()+"");
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textName;
        public TextView textMrp;
        public ImageButton btnAdd;

        public ViewHolder(View itemView) {
            super(itemView);

            textName = (TextView) itemView.findViewById(R.id.text_item_name);
            textMrp = (TextView) itemView.findViewById(R.id.text_item_mrp);
            btnAdd = (ImageButton) itemView.findViewById(R.id.btn_add_service);

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Service service = mList.get(getAdapterPosition());

                    if(toast!=null){
                        toast.cancel();
                    }
                    toast =Toast.makeText(view.getContext(), service.getName()+" added.", Toast.LENGTH_SHORT);
                    toast.show();

                    ContentValues cv = new ContentValues();
                    cv.put(DB.SaleItem.COL_ITEM_TYPE, ItemType.SERVICE);
                    cv.put(DB.SaleItem.COL_ITEM_ID, service.getServiceId());
                    cv.put(DB.SaleItem.COL_QUANTITY, 1);
                    cv.put(DB.SaleItem.COL_MRP, service.getMrp());

                    CartRepository.add(view.getContext(), cv);
                }
            });
        }
    }
}
