package dnet.org.bd.kallyanisales.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Mahabubul Hasan Uzzal <codehasan@gmail.com> on 12/5/2016.
 */

public class RetrofitClient {
    private static Retrofit retrofit;

    public static Retrofit getInstance(){
        if(retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://infolady.ht.dnetinsight.com/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}
